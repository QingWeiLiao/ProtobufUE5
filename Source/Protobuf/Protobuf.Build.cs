// Copyright Epic Games, Inc. All Rights Reserved.

using System;
using System.IO;
using UnrealBuildTool;

public class Protobuf : ModuleRules
{
	protected readonly string Version = "3.20.x";//版本号
	private  string ProtobufLib = "libprotobuf";//动态链接库,可选择,libprotobuf-lite ，libprotobuf
	
	public Protobuf(ReadOnlyTargetRules Target) : base(Target)
	{
		
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				// ... add other private include paths required here ...
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);


		Type = ModuleType.CPlusPlus;//ModuleType.External;CPlusPlus

		PublicDefinitions.Add("GOOGLE_PROTOBUF_NO_RTTI=1");
		PublicDefinitions.Add("GOOGLE_PROTOBUF_CMAKE_BUILD");

		PublicSystemIncludePaths.Add(Path.Combine(ModuleDirectory, "include"));
		
		//Console.WriteLine("DLL加载路径:"+Path.Combine(ModuleDirectory, "lib", "Windows", "libprotobuf.lib"));
		if (Target.Platform == UnrealTargetPlatform.Win64) {
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "lib", "Windows", ProtobufLib+".lib"));//libprotobuf-lite libprotobuf
		} else if (Target.Platform == UnrealTargetPlatform.IOS) {
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "lib", "IOS", ProtobufLib+".a"));
		} else if (Target.Platform == UnrealTargetPlatform.Android) {
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "lib", "Android", "ARMv7", ProtobufLib+".a"));
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "lib", "Android", "ARM64", ProtobufLib+".a"));
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "lib", "Android", "x64", ProtobufLib+".a"));
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "lib", "Android", "x86", ProtobufLib+".a"));
		} else if (Target.Platform == UnrealTargetPlatform.Mac) {
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "lib", "Mac", ProtobufLib+".a"));
		} else if (Target.Platform == UnrealTargetPlatform.Linux) {
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "lib", "Linux", ProtobufLib+".a"));
		}
	}
}
